import 'dart:io';
import 'Lottery.dart';

void main() {
  print("โปรแกรมตรวจล็อตเตอรี่ 16 สิงหาคม 2565"); //LotteryCheck
  stdout.write("ใส่เลขลอตเตอรี่: ");
  String input = stdin.readLineSync()!;
  if (checkLottery(input).isEmpty) {
    print("คุณไม่ถูกรางวัล");
  } else {
    for (var lt in checkLottery(input)) {
      stdout.write(lt);
    }
  }
}

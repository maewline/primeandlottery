import 'dart:io';

void primeNumber([int input = 0]) {
  bool check = false;
  if (input > 1) {
    for (var i = 2; i < input; i++) {
      if (input % i == 0) {
        check = true;
        break;
      }
    }
  }
  if (check == true) {
    print("$input is not a prime number");
  } else {
    print("$input is a prime number");
  }
}
